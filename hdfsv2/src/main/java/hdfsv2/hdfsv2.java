package hdfsv2;
 
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;

import java.util.Scanner;

public class hdfsv2 {
	
	public static String nombreArchivo;	
	public static String usuario;
	public static String grupo;
	public static String permissions;	
	public static String rutaHDFS ="/carpeta";
	public static String carpeta;
	

	public static void main(String[] args) {
		
		//Creamos la configuraci�n de acceso al HDFS
				Configuration conf = new Configuration(true);
				conf.set("fs.defaultFS", "hdfs://127.0.0.1:8020/");
				
				System.setProperty("HADOOP_USER_NAME", "hdfs");
				
		try {
			
			System.out.println("�En que ruta deseas guardar el archivo?");
	    	Scanner nuevaCarpeta = new Scanner(System.in);
	    	nuevaCarpeta.nextLine();
	    	carpeta=nuevaCarpeta.toString();
	    				
			System.out.println("Escribe el nombre del archivo");
	    	Scanner nombrearchivo = new Scanner(System.in);
	    	nombrearchivo.nextLine();
	    	nombreArchivo=nombrearchivo.toString();
	    		    	
			//Crear objeto FileSystem
			FileSystem fs = FileSystem.get(conf);
			String home = fs.getHomeDirectory().toString();
					
	    	//En caso de que no exista la carpeta, crear la carpeta.
			if(!fs.exists(new Path(home + "/" + carpeta))) {
				fs.mkdirs(new Path(home + "/" + carpeta));
			}
						
			//Si no existe el archivo, hay que crearlo
			Path rutaArchivo = new Path(home + "/" + carpeta + "/" + nombreArchivo);
			FSDataOutputStream outputStream = null;
			
			if(!fs.exists(rutaArchivo)) {
				outputStream = fs.create(rutaArchivo);
				outputStream.close();
			}
			
			//Tambien podemos modificar el propietario o los permisos del archivo.
			
			System.out.println("�Quien sera el propietario del arvchivo?");
	    	Scanner user = new Scanner(System.in);
	    	user.nextLine();
	    	usuario=user.toString();
	    	
	    	
	    	System.out.println("�Cual sera el grupo propietario del arvchivo?");
	    	Scanner group = new Scanner(System.in);
	    	group.nextLine();
	    	grupo=group.toString();
	    	
	    	
			fs.setOwner(rutaArchivo, usuario , grupo);
			
			System.out.println("Elija los permisos del archivo");
	    	Scanner permisos = new Scanner(System.in);
	    	permisos.nextLine();
	    	permissions=permisos.toString();
	    	
	    	permisos.close();
	    	group.close();
	    	user.close();
	    	nuevaCarpeta.close();
	    	nombrearchivo.close();
	    	
	    	FsPermission permis = FsPermission.valueOf("-rwxrwxrwx");
			fs.setPermission(rutaArchivo, permis);
			
			
			//Por ultimo, borraremos el directorio y los archivos.
			//fs.delete(new Path(route), true);
			//fs.close();
			
		}catch (IOException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}